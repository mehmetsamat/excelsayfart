import xlwt
import xlrd

from datetime import datetime

style0 = xlwt.easyxf('font: name Times New Roman, color-index red, bold on',
    num_format_str='#,##0.00')
style1 = xlwt.easyxf(num_format_str='D-MMM-YY')

okuwb = xlrd.open_workbook('example.xls')
okuwb.sheet_names()
okuhucre = okuwb.sheet_by_index(0)
okuhucre = okuwb.sheet_by_name("1")

ws = xlwt.Workbook('example.xls')
ws=okuwb
ws = ws.add_sheet("2")

ws.write(0, 0, 1234.56, style0)
ws.write(1, 0, datetime.now(), style1)
ws.write(2, 0, 1)
ws.write(2, 1, 1)
ws.write(2, 2, xlwt.Formula("A3+B3"))

okuwb.save('example.xls')
https://docs.aspose.com/display/cellsjava/Copying+and+Moving+Worksheets+in+Python
